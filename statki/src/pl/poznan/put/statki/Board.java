package pl.poznan.put.statki;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.SparseArray;

public class Board {
	private int boardSize = 10;
	public int cellSize;
	private SparseArray<SparseArray<Cell>> board = new SparseArray<SparseArray<Cell>>();
	private Paint paint = new Paint();

	public Board(DisplayMetrics metrics) {
		this.cellSize = metrics.widthPixels / 10;
		
		for (int row = 0; row < this.boardSize; row++) {
			SparseArray<Cell> rowMap = new SparseArray<Cell>();

			for (int col = 0; col < this.boardSize; col++) {
				rowMap.append(col, new Cell(this.cellSize, row, col));
			}
			this.board.append(row, rowMap);
		}
		
	}
	
	public void draw(Canvas canvas) {
		int x = 0;
		int y = 0;

		for (int i = 0; i < this.boardSize; i++) {
			SparseArray<Cell> row = this.board.get(i);
			y = i * this.cellSize;

			for (int j = 0; j < this.boardSize; j++) {
				x = j * this.cellSize;
				row.get(j).draw(canvas, this.paint, x, y);
			}
		}
	}
	
	public Cell getCell(int row, int col) {
		return this.board.get(row).get(col);
	}
	
	public int getCellSize() {
		return this.cellSize;
	}
}

class Cell {
	private int size = 10;
	private int backgroundColor = Color.GREEN;
	private boolean occupied = false;
	public int x = 0;
	public int y = 0;
	public int row = 0;
	public int col = 0;

	public Cell(int size, int row, int col) {
		this.size = size;
		this.row = row;
		this.col = col;
	}

	public void draw(Canvas canvas, Paint paint, int x, int y) {
		this.x = x;
		this.y = y;
		paint.setColor(Color.BLACK);
		canvas.drawRect(x, y, x + this.size, y + this.size, paint);
		paint.setColor(this.backgroundColor);
		canvas.drawRect(x + 1, y + 1, x + this.size - 2, y + this.size - 2,
				paint);
	}

	@Override
	public boolean equals(Object otherCell) {
		return (this.row == ((Cell) otherCell).row && this.col == ((Cell) otherCell).col);
	}

	@Override
	public int hashCode() {
		return Integer.parseInt(("" + this.row + this.col));
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

	public boolean isOccupied() {
		return this.occupied;
	}

	public boolean hasEmptyNeighbours(int neighboursCount, Board board) {
		if (this.getUnoccupiedNeighboursNorth(board).size() >= neighboursCount) {
			return true;
		}
		if (this.getUnoccupiedNeighboursEast(board).size() >= neighboursCount) {
			return true;
		}
		if (this.getUnoccupiedNeighboursSouth(board).size() >= neighboursCount) {
			return true;
		}
		if (this.getUnoccupiedNeighboursWest(board).size()  >= neighboursCount) {
			return true;
		}
		return false;
	}

	private ArrayList<Cell> getUnoccupiedNeighboursNorth(Board board) {
		ArrayList<Cell> unoccupiedNeighboursNorth = new ArrayList<Cell>();
		int northDistance = 1;

		while((this.row - northDistance) > 0) {
			Cell northCell = board.getCell(this.row - northDistance, this.col);

			if (northCell.isOccupied()) {
				break;
			}
			unoccupiedNeighboursNorth.add(northCell);
			northDistance++;
		}
		return unoccupiedNeighboursNorth;
	}

	private ArrayList<Cell> getUnoccupiedNeighboursEast(Board board) {
		ArrayList<Cell> unoccupiedNeighboursEast = new ArrayList<Cell>();
		int eastDistance = 1;

		while((this.col + eastDistance) < 10) {
			Cell eastCell = board.getCell(this.row, this.col + eastDistance);

			if (eastCell.isOccupied()) {
				break;
			}
			unoccupiedNeighboursEast.add(eastCell);
			eastDistance++;
		}
		return unoccupiedNeighboursEast;
	}

	private ArrayList<Cell> getUnoccupiedNeighboursSouth(Board board) {
		ArrayList<Cell> unoccupiedNeighboursSouth = new ArrayList<Cell>();
		int southDistance = 1;

		while((this.row + southDistance) < 10) {
			Cell southCell = board.getCell(this.row + southDistance, this.col);

			if (southCell.isOccupied()) {
				break;
			}
			unoccupiedNeighboursSouth.add(southCell);
			southDistance++;
		}
		return unoccupiedNeighboursSouth;
	}

	private ArrayList<Cell> getUnoccupiedNeighboursWest(Board board) {
		ArrayList<Cell> unoccupiedNeighboursWest = new ArrayList<Cell>();
		int westDistance = 1;

		while((this.col - westDistance) > 0) {
			Cell westCell = board.getCell(this.row, this.col - westDistance);

			if (westCell.isOccupied()) {
				break;
			}
			unoccupiedNeighboursWest.add(westCell);
			westDistance++;
		}
		return unoccupiedNeighboursWest;
	}
}
