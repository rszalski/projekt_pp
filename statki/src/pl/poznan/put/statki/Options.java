package pl.poznan.put.statki;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

public class Options extends Activity {
	
	static SharedPreferences settings;
	static SharedPreferences.Editor editor;
	
	private final BroadcastReceiver btReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        final String action = intent.getAction();

	        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
	            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
	                                                 BluetoothAdapter.ERROR);
	            if(state == BluetoothAdapter.STATE_ON){
	            	TextView tou = (TextView)findViewById(R.id.txt_options_username);
	            	CheckBox cnb = (CheckBox)findViewById(R.id.check_name_bluetooth);
	            	
	            	tou.setText(BluetoothAdapter.getDefaultAdapter().getName());
	            	tou.setEnabled(false);
	            	cnb.setChecked(true);
	            }
	        }
	    }
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_options);
		
		settings = getSharedPreferences("default", MODE_WORLD_WRITEABLE);
		editor = settings.edit();
		
		((TextView)findViewById(R.id.txt_options_username)).setText(settings.getString("nick", "myPhone"));
		((Spinner)findViewById(R.id.select_options_username_color)).setSelection(settings.getInt("color", 0));
		((SeekBar)findViewById(R.id.seek_options_sound_volume)).setProgress(settings.getInt("vol", 0));
		((ToggleButton)findViewById(R.id.toggle_options_vibrations)).setChecked(settings.getBoolean("vib", false));
		
		IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
	    this.registerReceiver(btReceiver, filter);
	}

	@Override
	public void onDestroy() {
		editor.putString("nick", Battle.myName = ((TextView)findViewById(R.id.txt_options_username)).getText().toString());
		editor.putInt("color", ((Spinner)findViewById(R.id.select_options_username_color)).getSelectedItemPosition());
		editor.putInt("vol", ((SeekBar)findViewById(R.id.seek_options_sound_volume)).getProgress());
		editor.putBoolean("vib", ((ToggleButton)findViewById(R.id.toggle_options_vibrations)).isChecked());
		editor.commit();
		
	    super.onDestroy();
	    this.unregisterReceiver(btReceiver);
	}
	
	public void useBluetoothName(View view) {
		CheckBox cb = (CheckBox)view;
		TextView tou = (TextView)findViewById(R.id.txt_options_username);
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if(cb.isChecked() && (btAdapter != null)){
			if(!btAdapter.isEnabled()){
				Intent enable_Bluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				cb.setChecked(false);
				startActivityForResult(enable_Bluetooth, 1);
			}
			else {
				findViewById(R.id.txt_options_username).setEnabled(false);
				tou.setText(btAdapter.getName());
			}
		}
		else{
			findViewById(R.id.txt_options_username).setEnabled(true);
			tou.setText(settings.getString("nick", "myPhone"));
		}
	}
}
