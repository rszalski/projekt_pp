package pl.poznan.put.statki;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.Toast;

class Ship {
	public int length;
	private Board board;
	protected Paint paint;
	private int cellSize;
	private int offset;
	public ArrayList<Cell> points;
	public ArrayList<Cell> hits;

	public Ship(int length, Board board, int cellSize) {
		this.board = board;
		this.paint = new Paint();
		
		switch (length) {
			case 1:
				this.paint.setColor(Color.RED);
				break;
			case 2:
				this.paint.setColor(Color.YELLOW);
				break;
			case 3:
				this.paint.setColor(Color.CYAN);
				break;
			case 4:
				this.paint.setColor(Color.WHITE);
				break;
		}
		this.points = new ArrayList<Cell>();
		this.hits = new ArrayList<Cell>();
		this.cellSize = cellSize;
		this.offset = 22;
		this.length = length;
		this.getShip();
	}

	public boolean inRange(Cell cell) {
		for (Cell point : this.points) {
			if (point.equals(cell)) {
				this.hits.add(point);
				return true;
			}
		}
		return false;
	}
	
	private Cell getFirstCell() {
		Random rand = new Random();
		Integer randomRow = 0;
		Integer randomCol = 0;		

		randomRow = rand.nextInt(9);
		randomCol = rand.nextInt(9);
		Cell cell = board.getCell(randomRow, randomCol);

		while (cell.isOccupied() && !cell.hasEmptyNeighbours(this.length - 1, board)) {
			randomRow = rand.nextInt(9);
			randomCol = rand.nextInt(9);
			cell = board.getCell(randomRow, randomCol);
		}
		cell.setOccupied(true);

		return cell;
	}

	public void getShip() {
		Cell firstCell = this.getFirstCell();
		this.points.add(firstCell);
				
		ArrayList<Cell> candidates = new ArrayList<Cell>();
		boolean candidatesGood = true;
		
		for (int j = 0; j < 4; j++) {			
			switch (j) {
				case 0:		// north
					for (int i = 1; i < this.length; i++) {
						if (firstCell.row - i > 9 || firstCell.row - i < 0) {
							candidatesGood = false;
							break;
						} else {
							candidates.add(board.getCell(firstCell.row - i, firstCell.col));
						}
					}
					break;
				case 1:		// east
					for (int i = 1; i < this.length; i++) {
						if (firstCell.col + i > 9 || firstCell.col + i < 0) {
							candidatesGood = false;
							break;
						} else {
							candidates.add(board.getCell(firstCell.row, firstCell.col + i));
						}
					}
					break;
				case 2:		// south
					for (int i = 1; i < this.length; i++) {
						if (firstCell.row + i > 9 || firstCell.row + i < 0) {
							candidatesGood = false;
							break;
						} else {
							candidates.add(board.getCell(firstCell.row + i, firstCell.col));
						}
					}
					break;
				case 3:		// west
					for (int i = 1; i < this.length; i++) {
						if (firstCell.col - i > 9 || firstCell.col - i < 0) {
							candidatesGood = false;
							break;
						} else {
							candidates.add(board.getCell(firstCell.row, firstCell.col - i));
						}
					}
					break;					
			}
			for (Cell candidateCell : candidates) {
				if (candidateCell.isOccupied()) {
					candidatesGood = false;
					break;
				}
			}
			if (candidatesGood) {
				break;
			}
			candidatesGood = true;
			candidates.clear();
		}
		for (Cell candidateCell : candidates) {
			candidateCell.setOccupied(true);
			this.points.add(candidateCell);
		}
	}

	public void draw(Canvas canvas) {
		for (Cell point : this.points) {
			if (this.hits.contains(point)) {
				// Change paint
				Paint paint2 = new Paint();
				paint2.setColor(Color.BLACK);
				canvas.drawCircle(point.x + this.offset, point.y + this.offset, this.offset, paint2);
			} else {
				canvas.drawCircle(point.x + this.offset, point.y + this.offset, this.offset, this.paint);
			}
		}
	}

	public void hit(Context context) {
		Toast.makeText(context, "Trafiony!", Toast.LENGTH_SHORT).show();
	}
}
