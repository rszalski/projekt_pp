package pl.poznan.put.statki;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Lobby extends Activity {
	
	ArrayList<String> arrayToListView;
    ArrayAdapter<String> arrayAdapter;
    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    
	private BattleService bService;
	private GameboardView gView = null;

    private static final String TAG = "Battle";
    private static final boolean D = true;

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lobby);
		
		arrayToListView = new ArrayList<String>();
		arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayToListView);
		ListView lv = (ListView)findViewById(R.id.list_lobby_participants);
        lv.setAdapter(arrayAdapter);
        arrayToListView.add(Battle.myName);
        arrayToListView.add(Battle.opponentName);
        
		bService = new BattleService(this, mHandler);
		bService.start();
	}
	
	public void displayGameboard(View view) {
		if(Battle.opponentAddress != null){
			BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(Battle.opponentAddress);
	        if(bService.getState() != bService.STATE_CONNECTED) bService.connect(device);
			Intent intent = new Intent(this, Gameboard.class);
			
			/*Bundle bundle = new Bundle();
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutput out = null;
			try {
			  out = new ObjectOutputStream(bos);   
			  out.writeObject(bService);
			  byte[] bServiceBytes = bos.toByteArray();
			  bundle.putByteArray(EXTRA_MESSAGE, bServiceBytes);
			  out.close();
			  bos.close();
			} catch (IOException e) {
				//pokemon
			}*/
			
			startActivity(intent);
		}
	}
	
    // The Handler that gets information back from the BattleService
    public final Handler mHandler = new Handler() {
    	
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
            	Context context = getApplicationContext();
            	CharSequence text = null;
            	int duration = Toast.LENGTH_SHORT;
                switch (msg.arg1) {
                case BattleService.STATE_CONNECTED:
                	displayGameboard(null);
                    break;
                case BattleService.STATE_CONNECTING:
                	text = "CONNECTING";
                    break;
                case BattleService.STATE_LISTEN:
                case BattleService.STATE_NONE:
                	text = "LISTEN";
                    break;
                }
            	Toast toast = Toast.makeText(context, text, duration);
            	toast.show();
                break;
            case MESSAGE_WRITE:
            	byte[] readBuf = (byte[]) msg.obj;
                String readMessage = new String(readBuf, 0, msg.arg1);
                Toast toast1 = Toast.makeText(getApplicationContext(), readMessage, Toast.LENGTH_SHORT);
            	toast1.show();
                break;
            case MESSAGE_READ:
            	readBuf = (byte[]) msg.obj;
                readMessage = new String(readBuf, 0, msg.arg1);
                toast1 = Toast.makeText(getApplicationContext(), readMessage, Toast.LENGTH_SHORT);
            	toast1.show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
}
