package pl.poznan.put.statki;

import android.os.Bundle;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;


public class Statki extends FragmentActivity {
	
	static SharedPreferences settings;
	static SharedPreferences.Editor editor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statki);
        
        settings = getSharedPreferences("default", MODE_WORLD_READABLE);
        editor = settings.edit();
        Battle.myName = settings.getString("nick", "myPhone");
    }
    
    public void displayInstructions(View view) {
    	DialogFragment frag = new InstructionsD();
    	frag.show(getSupportFragmentManager(), "instructions");
    }
    
    public void displayOptions(View view) {
    	Intent intent = new Intent(this, Options.class);
    	startActivity(intent);
    }
    
    public void displayAbout(View view) {
    	DialogFragment frag = new AboutD();
    	frag.show(getSupportFragmentManager(), "about");
    }
    
    public void displayNewGame(View view) {
    	BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    	
    	if(btAdapter != null){
    		if(!btAdapter.isEnabled()){
				Intent enable_Bluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enable_Bluetooth, 1);
    		}
    		else{
            	Intent intent = new Intent(this, NewGame.class);
            	startActivity(intent);
    		}
    	}
    }
    
    public void quit(View view) {
    	this.finish();
    }
}
