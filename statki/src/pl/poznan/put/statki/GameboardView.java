package pl.poznan.put.statki;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameboardView extends SurfaceView implements
		SurfaceHolder.Callback {
	private GameboardThread gameboardThread;
	private int xOffset = 0;
	private int yOffset = 0;
	private boolean isMoving = false;
	private Board board;
	private ArrayList<Ship> ships;
	private BattleService bService = null;

	
	public GameboardView(Context context, DisplayMetrics metrics, BattleService bs){
		super(context);

		this.board = new Board(metrics);
		this.createShips();

		getHolder().addCallback(this);
		this.gameboardThread = new GameboardThread(this);
		setFocusable(true);
		
		this.bService = bs;
		//bs.mHandler.gView = this;
	}

	public GameboardView(Context context, DisplayMetrics metrics) {
		super(context);

		this.board = new Board(metrics);
		this.createShips();

		getHolder().addCallback(this);
		this.gameboardThread = new GameboardThread(this);
		setFocusable(true);
	}
	
	private void createShips() {
		this.ships = new ArrayList<Ship>();
		
		for (int i = 1; i <= 4; i++) {
			this.ships.add(new Ship(1, this.board, this.board.getCellSize()));
		}		
		for (int i = 1; i <= 3; i++) {
			this.ships.add(new Ship(2, this.board, this.board.getCellSize()));
		}
		for (int i = 1; i <= 2; i++) {
			this.ships.add(new Ship(3, this.board, this.board.getCellSize()));
		}
		this.ships.add(new Ship(4, this.board, this.board.getCellSize()));
	}

	@Override
	public void onDraw(Canvas canvas) {
		this.board.draw(canvas);
		
		for (Ship ship : this.ships) {
			ship.draw(canvas);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			this.isMoving = false;
		} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			this.isMoving = true;
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			if (!this.isMoving) {
				int col = (int) Math
						.ceil(((this.xOffset + event.getX()) / this.board.getCellSize()) - 1);
				int row = (int) Math
						.ceil(((this.yOffset + event.getY()) / this.board.getCellSize()) - 1);
				Cell cell = this.board.getCell(row, col);

				for (Ship ship : this.ships) {
					if (ship.inRange(cell)) {
						ship.hit(getContext());
					}
				}
			}
		}
		return true;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	public void surfaceCreated(SurfaceHolder holder) {
		if (!this.gameboardThread.isAlive()) {
			this.gameboardThread = new GameboardThread(this);
		}
		this.gameboardThread.setRunning(true);
		this.gameboardThread.start();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		this.gameboardThread.setRunning(false);

		while (retry) {
			try {
				this.gameboardThread.join();
				retry = false;
			} catch (InterruptedException ex) {
				// try again and again
			}
		}
	}
}

class GameboardThread extends Thread {
	private GameboardView gameboardView;
	private boolean run = false;

	public GameboardThread(GameboardView gameboardView) {
		this.gameboardView = gameboardView;
	}

	public void setRunning(boolean run) {
		this.run = run;
	}

	@Override
	public void run() {
		Canvas canvas;

		while (this.run) {
			canvas = null;

			try {
				canvas = this.gameboardView.getHolder().lockCanvas(null);

				synchronized (this.gameboardView.getHolder()) {
					this.gameboardView.onDraw(canvas);

				}
			} finally {
				if (canvas != null) {
					this.gameboardView.getHolder().unlockCanvasAndPost(canvas);
				}
			}
		}
	}
}
