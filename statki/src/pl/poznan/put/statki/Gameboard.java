package pl.poznan.put.statki;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

public class Gameboard extends Activity {
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		BattleService bService = null;
		/*ByteArrayInputStream bis = new ByteArrayInputStream(savedInstanceState.getByteArray(Lobby.EXTRA_MESSAGE));
		ObjectInput in = null;
		try {
		  in = new ObjectInputStream(bis);
		  bService = (BattleService)in.readObject();
		  bis.close();
		  in.close();
		} catch (Exception e){
			//pokemon
		}*/

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		setContentView(new GameboardView(this, metrics, bService));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.activity_gameboard, menu);
		return true;
	}
}
