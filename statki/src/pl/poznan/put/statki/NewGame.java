package pl.poznan.put.statki;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

public class NewGame extends Activity {
	
	HashMap<String, BluetoothDevice> devices;
	ArrayList<String> arrayToListView;
    ArrayAdapter<String> arrayAdapter;

	private final BroadcastReceiver btReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        final String action = intent.getAction();

	        if(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(action)) {
	            
	            int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, 
	              BluetoothAdapter.ERROR);
	            CheckBox cnv = (CheckBox)findViewById(R.id.check_newgame_visible);
	            
	            switch(mode){
	            case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
	            	cnv.setChecked(true);
	            	cnv.setClickable(false);
	             break;
	            case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
	            case BluetoothAdapter.SCAN_MODE_NONE:
	            	cnv.setChecked(false);
	            	cnv.setClickable(true);
	             break;
	            }
	        } else if(BluetoothDevice.ACTION_FOUND.equals(action)){
	        	BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	        	
	        	if((device != null) && (device.getName() != null)){
		            arrayToListView.add(device.getName());
		            devices.put(device.getName(), device);
		            arrayAdapter.notifyDataSetChanged();
	        	}
	        }
	}};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_game);
		
		devices = new HashMap<String, BluetoothDevice>();
        arrayToListView = new ArrayList<String>();
        ListView lv = (ListView)findViewById(R.id.list_newgame_found_users);
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayToListView);
		
        lv.setAdapter(arrayAdapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
        	
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                
                Battle.opponentName = ((TextView)view).getText().toString();
                Battle.opponentAddress = devices.get(Battle.opponentName).getAddress();
                
                Intent intent = new Intent();
                intent.putExtra("device_address", Battle.opponentAddress);
                setResult(Activity.RESULT_OK, intent);
                displayLobby(view);
            }
        });
		
		registerReceiver(btReceiver, new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));
		registerReceiver(btReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
		
		checkVisibility(findViewById(R.id.check_newgame_visible));
	}
	
	@Override
	protected void onRestart() {
	    super.onRestart();
	    
	    devices.clear();
		arrayToListView.clear();
		arrayAdapter.notifyDataSetChanged();
	}

	public void displayLobby(View view) {
		Intent intent = new Intent(this, Lobby.class);
		startActivity(intent);
	}
	
	public void checkVisibility(View view){
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		CheckBox cnv = (CheckBox)view;
		
		if(btAdapter.getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE){
			cnv.setChecked(true);
			cnv.setClickable(false);
		}
		else if(cnv.isChecked()){
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE); 
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 120);
            startActivityForResult(discoverableIntent, 1);
            cnv.setChecked(false);
		}
	}
	
	public void findBluetoothDevices(View view){
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

		if(btAdapter.isDiscovering()) btAdapter.cancelDiscovery();
		
	    devices.clear();
		arrayToListView.clear();
		arrayAdapter.notifyDataSetChanged();
		
		btAdapter.startDiscovery();
	}
	
	@Override
	public void onDestroy() {
	    super.onDestroy();
	    this.unregisterReceiver(btReceiver);
	}
}